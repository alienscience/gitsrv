FROM alpine:3

RUN apk add --no-cache openssh git curl bash openssl
RUN ssh-keygen -A

WORKDIR /git-server/

RUN mkdir /git-server/keys \
  && adduser -D -s /usr/bin/git-shell git \
  && mkdir /home/git/.ssh \
  && mkdir /home/git/git-shell-commands

RUN echo "git:$(openssl rand -hex 20)" | chpasswd

COPY src/start.sh /start.sh
COPY src/gitconfig /home/git/.gitconfig
COPY hooks /home/git/hooks

EXPOSE 22

CMD ["sh", "/start.sh"]
