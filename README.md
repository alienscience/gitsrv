# gitsrv

Fork of [gitsrv](https://github.com/fluxcd/gitsrv) with personal patches.


## Usage

Clone the repo from another pod that has the same `ssh-key` secret mounted:

```bash
git clone -b master ssh://git@gitsrv/~/cluster.git
```

