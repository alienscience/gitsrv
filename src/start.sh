#!/usr/bin/env bash
set +x
set -o errexit
set -o pipefail
set -o nounset

CONFIG_DIR='/config'
PERSIST_DIR='/git-server'
AUTHORIZED_KEYS="$CONFIG_DIR/keys/authorized_keys"
HOST_KEYS="$PERSIST_DIR/host-keys"
REPOS="$PERSIST_DIR/repos"
REPO_DIR="$REPOS/$REPO"
GITHOOKS="/home/git/hooks"

# Copy git committer public keys
if [ "$(ls $AUTHORIZED_KEYS)" ]; then
  cd /home/git
  mkdir -p .ssh
  cp "$AUTHORIZED_KEYS" .ssh/authorized_keys
  chown -R git:git .ssh
  chmod 700 .ssh
  chmod -R 600 .ssh/*
fi

# If there are previous host-keys, copy them.
if [ "$(ls -A $HOST_KEYS/)" ]; then
  cp $HOST_KEYS/* /etc/ssh/
else
  ssh-keygen -A
  mkdir -p "$HOST_KEYS"
  cp /etc/ssh/ssh_host_* "$HOST_KEYS/"
fi

# Set permissions
if [ "$(ls -A $REPOS)" ]; then
  cd /git-server/repos
  chown -R git:git .
  chmod -R ug+rwX .
  find . -type d -exec chmod g+s '{}' +
fi

# Set seeder user name                                               
git config --global user.email "${GIT_USER_EMAIL:-root@gitsrv.git}"  
git config --global user.name "${GIT_USER_NAME:-root}"               
git config --global init.defaultBranch main

init_repo() {
  mkdir "${REPO_DIR}"
  cd "${REPO_DIR}"
  git init --bare --shared=true
  cp $GITHOOKS/* "${REPO_DIR}/hooks/"

  cd "$REPOS"
  chown -R git:git .
  chmod -R ug+rwX .
  find . -type d -exec chmod g+s '{}' +
}

if [ ! -d "${REPO_DIR}" ]; then
  init_repo
fi

# Link to home dir, this need to be done each time as this dir has the lifetime of the pod
ln -s "${REPO_DIR}" /home/git/

echo "Setup successful, starting SSH"

# -D flag avoids executing sshd as a daemon
/usr/sbin/sshd -D
